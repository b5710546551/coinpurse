package coinpurse;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.border.Border;
/**
 * 
 * @author Chinatip Vichian
 *
 */

public class PurseStatusObserver extends JFrame implements Observer{
	/** Constructor of PurseObserver*/
	private Container content;
	private Border border;
	private JProgressBar progressBar;
	public PurseStatusObserver() {
		super("Purse Status Observer");
		content = this.getContentPane();
	}
	/** Update receives notification from the purse*/
	public void update(Observable subject, Object info) {
		Purse purse = (Purse) subject;
		if(subject instanceof Purse){
			super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			progressBar = new JProgressBar(0,purse.getCapacity());
			progressBar.setStringPainted(true);
			if(purse.isFull())
				border = BorderFactory.createTitledBorder("Full");
			else
				border = BorderFactory.createTitledBorder("Empty");
			progressBar.setValue(purse.count());
			progressBar.setBorder(border);
			content.add(progressBar, BorderLayout.CENTER);
			super.setSize(300, 100);
			super.setVisible(true);
		}
	}
	public void run(){
		this.setSize(new Dimension(300,100));
		this.setVisible(true);
	}
}