package coinpurse;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A coupon with a monetary value.
 * You can't change the color of a coupon.
 * Can check the value of coupon by its color
 * This class implements interface Valuable.
 * 
 * @author Chinatip Vichian
 */
public class Coupon extends AbstractValuable {

	/* Color of the Coupon */
	private String color;
	

	
	/* Map that save the value of colors */
	private Map<String,Double> couponColor = new HashMap <String,Double>();

	/** 
	 * Constructor for a new Coupon. 
	 * @param color is color of the coupon
	 */
	public Coupon (String color2){
		this.color = color2;
		couponColor.put("red", 100.0);
		couponColor.put("blue", 50.0);
		couponColor.put("green", 20.0);
		
		/* to check that map contains  */
		color.toLowerCase();
		if (!couponColor.containsKey(color))
			color = null;
	}

	/**
	 * return String of the Coupon class
	 * @return String of color of Coupon
	 */
	public String toString(){
		return String.format("%s coupon", color);
	}

	/**
	 * Get value of the coupon
	 * @return value of Coupon
	 */
	public double getValue(){
		return couponColor.get(color);
		
	}
	/**
	 * Get currency of Banknote
	 * @return currency of money
	 */
	@Override
	public String getCurrency() {
		return "coupon";
	}
}
