package coinpurse;

import java.util.ResourceBundle;

import coinpurse.strategy.RecursiveWithdraw;
 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Chinatip Vichian
 */
public class Main {
	private static MoneyFactory instance;
    /**
     * @param args not used
     */
    public static void main( String[] args ) {
    	ResourceBundle bundle = ResourceBundle.getBundle("coinpurse.purse");
    	String location = bundle.getString("moneyfactory");
    	
    	try {
			instance = (MoneyFactory)Class.forName(location).newInstance();
			MoneyFactory.setMoneyFactory(instance);
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
        // 1. create a Purse
    		Purse purse = new Purse(20);
    		PurseBalanceObserver balance = new PurseBalanceObserver();
    		PurseStatusObserver status = new PurseStatusObserver();
    		purse.addObserver(balance);
    		purse.addObserver(status);
    		balance.run();
    		status.run();
    		purse.setWithdrawStrategy(new RecursiveWithdraw());
        // 2. create a ConsoleDialog with a reference to the Purse object
    		ConsoleDialog console = new ConsoleDialog (purse);
        // 3. run() the ConsoleDialog
    		console.run();
    }
}
