package coinpurse;

/**
 *  This interface help uses to avoid duplicate code.
 *  For Coin, Banknote and coupon have same thing in commons which is value.
 *  help to get the value of them and no need to add method getValue in each class
 *  
 *  @author Chinatip Vichian
 */
public interface Valuable extends Comparable<Valuable>{
	/**
	 * Call this method to get value of the object.
	 * @return value of the object in double
	 */
	public double getValue( );
	
	/**
	 * Get currency of money.
	 * @return currency of money
	 */
	public String getCurrency();
}