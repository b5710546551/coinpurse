package coinpurse;
/**
 * This class help create money in currecy Baht
 * @author Chinatip Vichian 5710546551
 *
 */
public class ThaiMoneyFactory extends MoneyFactory{
	/** Constructor of this class **/
	public ThaiMoneyFactory(){
		super();
	}

/**
 * This method creates money by receive value from parameter
 * @return Valuable interface which is the superclass for coin, banknote and coupon.
 */
	Valuable createMoney(double value) {
		Valuable valuable = null;
		if(value==1||value==2||value==5||value==10){
			valuable = new Coin(value,"Baht");
		}
		else if(value==20||value==50||value==100||value==500||value==1000){
			valuable = new BankNote(value,"Baht");
		}
		return valuable;
	}
}
