package coinpurse;
/**
 * This class helps creating money in currency Ringgit
 * @author Chinatip Vichian 5710546551
 *
 */
public class MalayMoneyFactory extends MoneyFactory{
	public MalayMoneyFactory(){
		super();
	}
	/**
	 * This method creates money by receive value from parameter
	 * @return Valuable interface which is the superclass for coin, banknote and coupon.
	 */
	Valuable createMoney(double value) {
		Valuable valuable = null;
		if(value==0.05||value==0.1||value==0.2||value==0.5){
			valuable = new Coin(value,"Sen");
		}
		else if(value==1||value==2||value==5||value==10||value==20||value==50||value==100){
			valuable = new BankNote(value,"Ringgit");
		}
		return valuable;
	}

}
