package coinpurse;

import java.util.Arrays;
import java.util.List;
/**
 * ListUtil i s a class that contain printList method that use recursion instead of loop.
 * Moreover, it has helper method for printList method(Recursion) which is sumOf method.
 * 
 * @author Chinatip Vichian
 *
 */
public class ListUtill {
	/**
	 * Print elements in the list
	 * 
	 * @param list is list of things that will be printed
	 */
	public static void printList(List<?> list){
		if(1==list.size()) System.out.print(list.get(0));
		else{
			System.out.print(list.get(0)+", ");
			printList(list.subList(1,list.size()));
		}
		
	}


	/**
	 * Find the largest element in aLIst of Strings.
	 * using the String compareTo method.
	 * 
	 * @param list of string used to find the largest element 
	 * @return the lexically largest element in the List
	 */
	public static String max(List<String> list){
		if(1==list.size()) return list.get(0);
		if(list.get(0).compareTo(list.get(list.size()-1))>0)
			return max(list.subList(0,list.size()-1));	
		else
			return max(list.subList(1,list.size()));
	}

	/** Test the max method **/
	public static void main (String args[]){
		List<String> list;

		if (args.length>0) list = Arrays.asList(args);
		else list = Arrays.asList("bird","zebra","cat","pig");

		System.out.print("List contains: ");
		printList(list);

		String max = max(list);
		System.out.println("\nLexically greatest element is "+max);
	}

}
