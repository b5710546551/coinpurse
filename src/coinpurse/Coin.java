package coinpurse;

/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * 
 * 
 * @author Chinatip Vichian
 */
public class Coin extends AbstractValuable { 

	/** Value of the coin */
	double value;
	/** currency of money **/
	private String currency ="";
	/** 
	 * Constructor for a new coin. 
	 * @param value is the value for the coin
	 */
	public Coin( double value ,String currency) {
		this.value = value;
		this.currency = currency;
	}

	/**
	 * get the value of this coin
	 * @return value of the coin
	 */
	public double getValue(){	return this.value;	}

	/**
	 * return String of the coin class
	 * @return String of value and currency of Baht ex. "2 Baht"
	 */
	public String toString(){
		return String.format("%.0f %s coin",value ,currency);
	}
	/**
	 * Get currency of Banknote
	 * @return currency of money
	 */
	@Override
	public String getCurrency() {
		return currency;
	}
}