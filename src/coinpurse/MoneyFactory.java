package coinpurse;
/**
 * This is the superclass for ThaiMoneyFactoru and MalayFactory that helps creates money.
 * @author Chinatip Vichian 5710546551
 *
 */
public abstract class MoneyFactory {
	/** Instance of MoneyFactory class **/
	private static MoneyFactory factory;
	
	/**
	 * Get instance of money factory
	 * @return instance of MoneyFactory
	 */
	static MoneyFactory getInstance() {
		return factory;
	}
	
	/**
	 * Set Instance or currency by setting this method
	 * @param fac can be ThaiMoneyFactory or MalayMoneyFactory
	 */
	static void setMoneyFactory(MoneyFactory fac){
		factory = fac;
	}
	/**
	 * This method creates money by receive value from parameter
	 * @return Valuable interface which is the superclass for coin, banknote and coupon.
	 */
	abstract Valuable createMoney(double value);

	/**
	 * This method creates money by receive value in String from parameter
	 * @return Valuable interface which is the superclass for coin, banknote and coupon.
	 */
	Valuable createMoney(String value) {
		return createMoney(Double.parseDouble(value));
	}
}
