package coinpurse.strategy;
import java.util.ArrayList;
import java.util.List;
import coinpurse.Valuable;
/**
 * A RecursiveWithdraw class implements WithdrawStrategy that have withdraw method.
 * This class will specify action of withdrawal money.
 * Furthermore, this can withdraw some amount that GreedyWithdraw cannot do.
 * 
 * @author Chinatip Vichian
 */
public class RecursiveWithdraw implements WithdrawStrategy{
	/** List of withdrawn Valuables */
	private List<Valuable> withdrawnList;

	/**
	 * Get array of the money that was withdrawn each time.
	 * @return array of Valuable
	 */
	public Valuable[] getWithdrawnList(){
		Valuable[] withdrawArray = new Valuable[withdrawnList.size()];
		withdrawArray = withdrawnList.toArray(withdrawArray);
		return withdrawArray;
	}

	/**
	 * Withdraw method will remove valuable from the List it possible.
	 * If coins, coupons and banknotes overall value and its number do not match with the amount, 
	 * then cannot withdraw the money.
	 * 
	 * @param amount is amount of money that wanted to be withdraw.
	 * @param valuables is a list of valuables.
	 */
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		withdrawnList= new ArrayList<Valuable>();

		Valuable[] array =  recursiveWithdraw(amount, valuables, 0, 0);
		return array;
	}

	/**
	 * RecursiveWithdraw can check every case that money can be withdraw.
	 * @param amount is amount of money that wanted to be withdraw.
	 * @param valuables is a list of valuables.
	 * @param sum is sum of all valuables collect from last recursion
	 * @param index helps run recursion
	 * @return Array of withdrawn Valuables
	 */
	public Valuable[] recursiveWithdraw(double amount, List<Valuable> valuables, double sum, int index){
		if(amount == sum){
			return  getWithdrawnList();
		}
		if(index>=valuables.size()){
			return null;
		}
		if(sum + valuables.get(index).getValue() > amount){

			return null;
		}
		else if(sum + valuables.get(index).getValue() <= amount){
			withdrawnList.add(valuables.get(index));
			sum+=valuables.get(index).getValue();
		}
		Valuable[] newValuable = recursiveWithdraw(amount, valuables, sum, index+1);
		if(newValuable==null){
			sum-= withdrawnList.get(withdrawnList.size()-1).getValue();
			withdrawnList.remove(withdrawnList.size()-1);
			return recursiveWithdraw(amount, valuables, sum, index+1);
		}
		return newValuable;
	}
}