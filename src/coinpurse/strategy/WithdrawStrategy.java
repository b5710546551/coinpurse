package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;
/**
 * A WithdrawStrategy interface help to collect withdraw methods, but they have different action.
 * 
 * @author Chinatip Vichian
 */
public interface WithdrawStrategy {
	/**
	 * Withdraw method will remove valuable from the List it possible.
	 * If coins, coupons and banknotes overall value and its number do not match with the amount, 
	 * then cannot withdraw the money.
	 * 
	 * @param amount is the value of money that wanted to withdraw
	 * @param valuables is a list of valuables
	 */
	public Valuable[] withdraw (double amount, List<Valuable> valuables);
}
