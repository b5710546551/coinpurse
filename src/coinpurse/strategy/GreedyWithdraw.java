package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.ValueComparator;
/**
 * A GreedyWithdraw class implements WithdrawStrategy that have withdraw method.
 * This class will specify action of withdrawal money.
 * 
 * @author Chinatip Vichian
 */
public class GreedyWithdraw implements WithdrawStrategy{
	
	/**
	 * Withdraw method will remove valuable from the List it possible.
	 * If coins, coupons and banknotes overall value and its number do not match with the amount, 
	 * then cannot withdraw the money.
	 * 
	 * @param amount is amount of money that wanted to be withdraw.
	 * @param valuables is a list of valuables.
	 */
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		Collections.sort(valuables,new ValueComparator());
		List<Valuable> withdrawList = new ArrayList<Valuable>();
		// if ( ??? ) return ???;

		/*
		 * One solution is to start from the most valuable coin in the purse and
		 * take any coin that maybe used for withdraw. Since you don't know if
		 * withdraw is going to succeed, don't actually withdraw the coins from
		 * the purse yet. Instead, create a temporary list. Each time you see a
		 * coin that you want to withdraw, add it to the temporary list and
		 * deduct the value from amount. (This is called a "Greedy Algorithm".)
		 * Or, if you don't like changing the amount parameter, use a local
		 * total to keep track of amount withdrawn so far.
		 * 
		 * If amount is reduced to zero (or tempTotal == amount), then you are
		 * done. Now you can withdraw the coins from the purse. NOTE: Don't use
		 * list.removeAll(templist) for this becuase removeAll removes *all*
		 * coins from list that are equal (using Coin.equals) to something in
		 * templist. Instead, use a loop over templist and remove coins
		 * one-by-one.
		 */

		double sum = 0;
		double temp = amount;
		for (int i = valuables.size() - 1; i >= 0; i--) {
			if (valuables.get(i).getValue() <= (amount - sum)) {
				temp -= valuables.get(i).getValue();
				sum += valuables.get(i).getValue();
				withdrawList.add(valuables.get(i));
			}
		}

		// Did we get the full amount?
		if (temp > 0) { // failed. Since you haven't actually remove
			// any coins from Purse yet, there is nothing
			// to put back.
			System.out.printf("Can't withdraw exactly %.0f Baht.\n", amount);
		}
		else {
			// Success.
			// Since this method returns an array of Coin,
			// create an array of the correct size and copy
			// the Coins to withdraw into the array.
			Valuable[] withdrawnList = new Valuable[withdrawList.size()];
			withdrawList.toArray(withdrawnList);
			return withdrawnList;
		}
		return null;
	}
}
