package coinpurse;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Panel;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * 
 * @author Chinatip Vichian
 *
 */
public class PurseBalanceObserver extends JFrame implements Observer{
	/** Constructor of PurseObserver*/
	private Container content;
	private Panel panel;
	private JLabel label;
	public PurseBalanceObserver() {
		super("Purse Balance Observer");
		content = this.getContentPane();
		panel = new Panel();
		Font f = new Font("Arial", Font.BOLD, 20);
		label = new JLabel("0 Baht");
		label.setAlignmentX(CENTER_ALIGNMENT);
		label.setAlignmentY(CENTER_ALIGNMENT);
		label.setFont(f);
		panel.add(label);
		content.add(panel);

	}
	/** Update receives notification from the purse*/
	public void update(Observable subject, Object info) {
		Purse purse = (Purse) subject;
		if(subject instanceof Purse){
			int balance = purse.getBalance();
			label.setText(balance +" Baht");
		}
	
	}
	public void run(){
		this.setSize(new Dimension(300,100));
		this.setVisible(true);
	}

}