package coinpurse;

import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;



/**
 * A coin purse contains coins. You can insert coins, withdraw money, check the
 * balance, and check if the purse is full. When you withdraw money, the coin
 * purse decides which coins to remove.
 * 
 * @author Chinatip Vichian
 */
public class Purse extends Observable{
	/** Collection of variable in the purse. */
	private List<Valuable> money;
	/** WithdrawStrategy for Purse */
	private WithdrawStrategy strategy = new RecursiveWithdraw();
	/**
	 * Capacity is maximum NUMBER of coins the purse can hold. Capacity is set
	 * when the purse is created.
	 */
	private int capacity;

	/**
	 * Create a purse with a specified capacity.
	 * 
	 * @param capacity
	 *            is maximum number of coins you can put in purse.
	 */
	public Purse(int capacity) {
		this.capacity = capacity;
		money = new ArrayList<Valuable>();
	}

	/**
	 * Count and return the number of coins in the purse. This is the number of
	 * coins, not their value.
	 * 
	 * @return the number of coins in the purse
	 */
	public int count() {
		return money.size();
	}

	/**
	 * Get the total value of all items in the purse.
	 * 
	 * @return the total value of items in the purse.
	 */
	public int getBalance() {
		double sum = 0;
		for (int i = 0; i < money.size(); i++) {
			sum += money.get(i).getValue();
		}
		return (int)sum;
	}

	/**
	 * Return the capacity of the coin purse.
	 * 
	 * @return the capacity
	 */
	public int getCapacity() {
		return this.capacity;
	}

	/**
	 * Test whether the purse is full. The purse is full if number of items in
	 * purse equals or greater than the purse capacity.
	 * 
	 * @return true if purse is full.
	 */
	public boolean isFull() {
		if (money.size() >= capacity)
			return true;
		return false;
	}

	/**
	 * Insert a valuable into the purse. The coin is only inserted if the purse has
	 * space for it and the valuable has positive value. No worthless valuable!
	 * 
	 * @param valuable
	 *            is a valuable object to insert into purse
	 * @return true if valuable inserted, false if can't insert
	 */
	public boolean insert(Valuable amount) {
		// if the purse is already full then can't insert anything.
		if (isFull() || amount.getValue() <= 0)
			return false;
		money.add(amount);
		super.setChanged();
		super.notifyObservers(this);
		return true;
	}

	/**
	 * Withdraw the requested amount of money. Return an array of Valuable
	 * withdrawn from purse, or return null if cannot withdraw the amount
	 * requested.
	 * 
	 * @param amount
	 *            is the amount to withdraw
	 * @return array of valuable objects for money withdrawn, or null if cannot
	 *         withdraw requested amount.
	 */
	public Valuable[] withdraw(double amount) {
		if(strategy.withdraw(amount, money)!=null){
			Valuable[] withdrawList = strategy.withdraw(amount, money);
			for (int j = 0; j < withdrawList.length; j++) {
				for (int h = 0; h < money.size(); h++) {
					if (withdrawList[j].equals(money.get(h)))
						money.remove(h);
				}
			}
			super.setChanged();
			super.notifyObservers(this);
			return withdrawList;
		}
		return null;
	}

	/**
	 * toString returns a string description of the purse contents. It can
	 * return whatever is a useful description.
	 */
	public String toString() {
		return money.size() + " coins with value " + getBalance();
	}

	/**
	 * Set Strategy to withdraw. ex. GreedyWithdraw or RecursiveWithdraw
	 * @param strategy is WithdrawStrategy for Purse
	 */
	public void setWithdrawStrategy(WithdrawStrategy strategy){
		this.strategy = strategy;
	}

}