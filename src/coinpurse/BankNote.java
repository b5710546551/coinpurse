package coinpurse;
/**
 *  A Banknote with a monetary value.
 *  You can't change the value of a banknote.
 *  Every banknote has its own serial number and always difference from others.
 *  This class implements interface Valuable.
 *  
 *  @author Chinatip Vichian
 */
public class BankNote extends AbstractValuable {

	/** Value of the BankNote */
	private double value;
	/**  a unique serial number, starting from 1,000,000 */
	private Long serialNumber;
	/**  static number that used to run serial number */
	private static Long nextSerialNumber = 999999L;
	/** currency of money **/
	private String currency ="";

	/** 
	 * Constructor for a new BankNote. 
	 * @param value is the value for the BankNote
	 */
	public BankNote(double value,String currency){
		this.value = value;
		this.currency = currency;
		serialNumber = getNextSerialNumber();
	}

	/**
	 * Use this method as a get method to get the value
	 * @return value of BankNote
	 */
	public double getValue(){
		return this.value;
	}
	
	
	/**
	 * Get serial number of the banknote
	 * @return serial number
	 */
	private static Long getNextSerialNumber(){
		return nextSerialNumber++;
	}
	
	/**
	 * return String of the Banknote class
	 * @return String of value and serial number of Banknote
	 */
	public String toString(){
		return String.format("%.0f %s Banknote [%d]",value, currency,nextSerialNumber);
	}
	/**
	 * Get currency of Banknote
	 * @return currency of money
	 */
	@Override
	public String getCurrency() {
		return currency;
	}

}
