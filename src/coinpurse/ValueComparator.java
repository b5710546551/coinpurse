package coinpurse;

import java.util.Comparator;

/**
 * ValueComparator is a class that work like Comparable interface, but no need to write override method.
 * 
 * @author Chinatip Vichian
 */
public class ValueComparator  implements Comparator<Valuable> {
	/**
	 * Compare two valuables
	 * 
	 * @param a is valuable that wanted to compare with another.
	 * @param b is valuable that wanted to compare with another
	 * @return a.) -1 if value of a is more than b
	 * b.) 0 if value of a and b are equal
	 * c.) 1 if value of b is more than a
	 */
	public int compare(Valuable a, Valuable b)  {
		if(a.getValue()>b.getValue())
			return -1;
		if(a.getValue()<b.getValue())
			return 1;
		return 0;
		}
}
