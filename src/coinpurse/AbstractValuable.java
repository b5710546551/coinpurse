package coinpurse;
/**
 *  This class checks whether two objects are equal and can compare value of objects.
 *  
 *  @author Chinatip Vichian
 */
public abstract class AbstractValuable implements Valuable{
	/* Constructor of AbstractValuable class*/
	public AbstractValuable() {
		super();
	}

	/**
	 * This method whether the two Valuables are equal
	 * @return true if a) arg is not null, b) arg is a Valuable, 
	 * c) arg has same value as this Valuable. false if it not
	 */
	public boolean equals(Object other) {
		if(other == null)
			return false;
		if(other.getClass() != this.getClass())
			return false;
	
		if ( this.getValue() ==  ((Valuable)other).getValue() )
			return true;
		return false; 
	}
	
	/**
	 * compare Valuables, so that the smaller value comes first
	 * @return -1 if Valuable1 has less value than Valuable2
	 * 0 if Valuable1 and Valuable2 have same value
	 * 1 if Valuable1 has greater value than Valuable2
	 */
	public int compareTo(Valuable other){
		if(this.getValue() < ((Valuable)other).getValue()) return -1;
		else if(this.getValue() == ((Valuable)other).getValue()) return 0;
		else return 1;
	}

}